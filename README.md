Another SNAKE
=============

![alt tag](https://github.com/macfij/another-snake/blob/master/images/snake_in.png)

This is a snake game made for fun by two programming noobs.
Linux, C++, SDL.

Features
========
- file based high scores
- modifiable snake speed and size
- full screen support
- wsad/arrows snake control
- background black/white

Snake changes his color based on coordinates. He has some similiarities with chameleon.
Image below presents snake just before his lunch. See how colorful he is?

![alt_tag](https://github.com/macfij/another-snake/blob/master/images/snake_game.png)

Authors
=======
- Jakub Rewieński
- Maciek Fijałkowski

